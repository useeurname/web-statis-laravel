@extends('template.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>{{$pertanyaan -> judul}}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/pertanyaan">Home</a></li>
          <li class="breadcrumb-item active">Pertanyaan {{$pertanyaan -> id}}</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
        <!-- Default box -->
        <div class="card">
            <div class="card-body">
                {{$pertanyaan -> isi}}
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Ditanyakan oleh: {{$pertanyaan -> profil_id}} <br> Pada Tanggal: {{$pertanyaan -> created_at}} <br> Terakhir Diubah Pada Tanggal: {{$pertanyaan -> updated_at}}
            </div>
        <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </div>
  </section>
@endsection