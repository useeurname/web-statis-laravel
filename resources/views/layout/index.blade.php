@extends('template.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Home</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Home</li>
          </ol>
        </div>
      </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Data Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                    <th>ID</th>
                    <th>Judul</th>
                    <th>Isi Pertanyaan</th>
                    <th>Tanggal Dibuat</th>
                    <th>Tanggal Diubah</th>
                    <th>Pembuat Pertanyaan</th>
                    <th>Jawaban Tepat</th>
                    <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($pertanyaan as $key => $pertanyaan)
                            <tr>
                                <td>{{$pertanyaan -> id}}</td>
                                <td>{{$pertanyaan -> judul}}</td>
                                <td>{{$pertanyaan -> isi}}</td>
                                <td>{{$pertanyaan -> created_at}}</td>
                                <td>{{$pertanyaan -> updated_at}}</td>
                                <td>{{$pertanyaan -> profil_id}}</td>
                                <td>{{$pertanyaan -> jawaban_tepat_id}}</td>
                                <td style="display: flex; justify-content: center; align-items: center;">
                                <a href="/pertanyaan/{{$pertanyaan -> id}}" class="btn btn-info btn-sm" style="margin-left: 3px; margin-right: 3px;">Tampilkan</a>
                                <a href="/pertanyaan/{{$pertanyaan -> id}}/edit" class="btn btn-default btn-sm" style="margin-left: 3px; margin-right: 3px;">Edit</a>
                                <form action="/pertanyaan/{{$pertanyaan -> id}}" method="post" style="margin-left: 3px; margin-right: 3px;">
                                  @csrf
                                  @method('DELETE')
                                  <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                                </form>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
    
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush