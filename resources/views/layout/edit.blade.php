@extends('template.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit Form</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/pertanyaan">Home</a></li>
            <li class="breadcrumb-item active">Edit Pertanyaan {{$pertanyaan -> id}}</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan {{$pertanyaan -> id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{$pertanyaan -> id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judulPertanyaan" name="judulPertanyaan" value="{{$pertanyaan -> judul}}" placeholder="Masukkan judul" required>
                    
                    @error('judulPertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <textarea class="form-control" id="isiPertanyaan" name="isiPertanyaan" placeholder="Masukkan pertanyaan anda"  style="margin-top: 0px; margin-bottom: 0px; height: 87px;" required>{{$pertanyaan -> isi}}</textarea>
                    @error('isiPertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
            </div>
        </form>
        </div>
        <!-- /.card -->
    </div><!-- /.container-fluid -->
</section>
@endsection