<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('layout.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judulPertanyaan' => 'required',
            'isiPertanyaan' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judulPertanyaan"],
            "isi" => $request["isiPertanyaan"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dibuat!');
    }

    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('layout.index', compact('pertanyaan'));
    }

    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('layout.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('layout.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judulPertanyaan' => 'required',
            'isiPertanyaan' => 'required'
        ]);

        $query = DB::table('pertanyaan')->where('id', $id)->update([
            'judul' => $request["judulPertanyaan"],
            'isi' => $request["isiPertanyaan"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diupdate!');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
